import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CommonService } from '../service/common.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
declare let Email: any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title = 'Jugadu Baba';
  serviceList = [];
  topList = [];
  msgForm: any;

  constructor(private router: Router, private commonSvc: CommonService, private fb: FormBuilder) { }

  ngOnInit() {
    this.msgForm = this.fb.group({
      name: ["", Validators.compose([Validators.required, Validators.minLength(4)])],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });

    this.serviceList = this.commonSvc.getAllData();

    this.topList = this.commonSvc.getTopList();
  }

  goToPage(data) {
    this.router.navigate(['/details', { id: data }]);
  }

  onSubmit() {
    if (this.msgForm.value) {
      Email.send({
        Host: 'smtp.elasticemail.com',
        Username: 'jugadubabaservices@gmail.com',
        Password: '91AB8A847272F4975158DEC6B3E30B69765F',
        To: 'jugadubabaservices@gmail.com',
        From: 'jugadubabaservices@gmail.com',
        Subject: 'General Query | ' + new Date(),
        Body: `
        <i>Query Information.</i>
         <br/> 
         <br/> 
         <br/> 
         <b>Name:</b>${this.msgForm.get('name').value}  <br />
         <b>Email:</b>  <a href = "mailto: ${this.msgForm.get('email').value}">${this.msgForm.get('email').value}</a> <br/> 
         <b>Subject:</b>${this.msgForm.get('subject').value}<br/>
         <b>Message:</b>${this.msgForm.get('message').value}<br/>
         `
      }).then(message => { alert('Form Submitted. We will contact you soon.'); this.msgForm.reset(); });
    }
  }



}
