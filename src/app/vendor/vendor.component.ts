import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
declare let Email: any;

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.scss']
})
export class VendorComponent implements OnInit {

  regForm: any;

  stateList = [

    {
      "key": "Andhra Pradesh",
      "val": "Hyderabad"
    },
    {
      "key": "Arunachal Pradesh",
      "val": "Itanagar"
    },
    {
      "key": "Assam",
      "val": "Dispur"
    },
    {
      "key": "Bihar",
      "val": "Patna"
    },
    {
      "key": "Chhattisgarh",
      "val": "Raipur"
    },
    {
      "key": "Goa",
      "val": "Panaji"
    },
    {
      "key": "Gujarat",
      "val": "Gandhinagar"
    },
    {
      "key": "Haryana",
      "val": "Chandigarh"
    },
    {
      "key": "Himachal Pradesh",
      "val": "Shimla"
    },
    {
      "key": "Jammu & Kashmir",
      "val": "Srinagar(Summer)/Jammu(Winter)"
    },
    {
      "key": "Jharkhand",
      "val": "Ranchi"
    },
    {
      "key": "Karnataka",
      "val": "Bengaluru"
    },
    {
      "key": "Kerala",
      "val": "Thiruvananthapuram"
    },


    {
      "key": "Madhya Pradesh",
      "val": "Bhopal"
    },
    {
      "key": "Maharashtra",
      "val": "Mumbai"
    },
    {
      "key": "Manipur",
      "val": "Imphal"
    },
    {
      "key": "Meghalaya",
      "val": "Shillong"
    },
    {
      "key": "Mizoram",
      "val": "Aizawl"
    },
    {
      "key": "Nagaland",
      "val": "Kohima"
    },
    {
      "key": "Odisha",
      "val": "Bhubaneswar"
    },
    {
      "key": "Punjab",
      "val": "Chandigarh"
    },
    {
      "key": "Rajasthan",
      "val": "Jaipur"
    },
    {
      "key": "Sikkim",
      "val": "Gangtok"
    },
    {
      "key": "Tamil Nadu",
      "val": "Chennai"
    },
    {
      "key": "Telangana",
      "val": "Hyderabad"
    },
    {
      "key": "Tripura",
      "val": "Agartala"
    },
    {
      "key": "Uttar Pradesh",
      "val": "Lucknow"
    },
    {
      "key": "Uttarakhand",
      "val": "Dehradun"
    },
    {
      "key": "West Bengal",
      "val": "Kolkata"
    },

    {
      "key": "Andaman & Nicobar Islands",
      "val": "Port Blair"
    },
    {
      "key": "Chandigarh",
      "val": "Chandigarh"
    },
    {
      "key": "Dadra & Nagar Haveli",
      "val": "Silvassa"
    },
    {
      "key": "Daman & Diu",
      "val": "Daman"
    },
    {
      "key": "Lakshadweep",
      "val": "Kavaratti"
    },
    {
      "key": "Puducherry",
      "val": "Puducherry"
    },
    {
      "key": "Delhi",
      "val": "New Delhi"
    }
  ]

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.regForm = this.fb.group({
      name: ["", Validators.compose([Validators.required, Validators.minLength(4)])],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      mobile: ["", Validators.compose([Validators.required, Validators.maxLength(10)])],
      experince: ['', Validators.required],
      address: ['', Validators.required],
      address2: [''],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zip: ['', Validators.compose([Validators.required, Validators.maxLength(6)])]
    });
  }

  onSubmit() {
    if (this.regForm.value) {
      Email.send({
        Host: 'smtp.elasticemail.com',
        Username: 'jugadubabaservices@gmail.com',
        Password: '91AB8A847272F4975158DEC6B3E30B69765F',
        To: 'jugadubabaservices@gmail.com',
        From: 'jugadubabaservices@gmail.com',
        Subject: 'Vendor Registration | ' + new Date(),
        Body: `<i>Vendor Information.</i> <br/> <b>Name:</b> ${this.regForm.get('name').value} <br/> <b>Email:</b>  <a href = "mailto: ${this.regForm.get('email').value}">${this.regForm.get('email').value}</a> <br/>  <b> Mobile:</b>${this.regForm.get('mobile').value}<br/><b>Service Field:</b> ${this.regForm.get('experince').value}<br/><b>Address:</b> ${this.regForm.get('address').value}  <br/><b>Address2:</b> ${this.regForm.get('address2').value}  <br/> <b>City:</b> ${this.regForm.get('city').value} <br/> <b>State:</b> ${this.regForm.get('state').value} <br/> <b>zip:</b> ${this.regForm.get('zip').value} <br/> <br> `
      }).then(message => { alert('Thanks for registration. We will contact you soon.'); this.regForm.reset(); });
    }




  }
}



