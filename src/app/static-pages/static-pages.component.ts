import { Component, OnInit } from '@angular/core';
import { CommonService } from '../service/common.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-static-pages',
  templateUrl: './static-pages.component.html',
  styleUrls: ['./static-pages.component.scss']
})
export class StaticPagesComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private commonSvc: CommonService) {

  }

  details: any;
  href: any;

  serviceList = [];

  ngOnInit() {
    this.href = this.router.url;

    if (this.href && this.href !== "/services") {
      this.details = this.commonSvc.getContent(this.href);
    } else if (this.href === "/services") {
      this.details = { title: "Services" }
      this.serviceList = this.commonSvc.getAllData();
    }

  }
  goToPage(data) {
    this.router.navigate(['/details', { id: data }]);
  }

}
