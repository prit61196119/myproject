import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  styles:[`.router-link-active { background-color: red; }`]
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  activeRoute:any;

  ngOnInit() {
  }

  ngDoCheck(){
    this.activeRoute = this.router.url;

  }

  
}
