import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../service/common.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private router: Router, private commonSvc: CommonService) { }

topList = [];
  ngOnInit() {
      this.topList = this.commonSvc.getTopList();
  }

  goToPage(data) {
    this.router.navigate(['/details', { id: data }]);
  }

}
