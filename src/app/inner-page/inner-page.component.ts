import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../service/common.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-inner-page',
  templateUrl: './inner-page.component.html',
  styleUrls: ['./inner-page.component.scss']
})
export class InnerPageComponent implements OnInit {
  detailData: any = '';

  constructor(private route: ActivatedRoute, private router: Router, private commonSvc: CommonService) {

  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    console.log('id', id)
    if (id) {
      this.detailData = this.commonSvc.getDataById(id);
    }
  }

}
