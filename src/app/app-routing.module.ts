import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InnerPageComponent } from './inner-page/inner-page.component';
import { HomeComponent } from './home/home.component';
import { VendorComponent } from './vendor/vendor.component';
import { StaticPagesComponent } from './static-pages/static-pages.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  { path: 'home', redirectTo: '', pathMatch: 'full' },
  {
    path: 'details',
    component: InnerPageComponent,
  },
  {
    path: 'vendor',
    component: VendorComponent,
  },
  {
    path: 'about',
    component: StaticPagesComponent,
  },
  {
    path: 'services',
    component: StaticPagesComponent,
  },
    {
    path: 'terms-and-conditions',
    component: StaticPagesComponent,
  },
  {
    path: 'privacy-policy',
    component: StaticPagesComponent,
  },
  {
    path: 'contact-us',
    component: StaticPagesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
