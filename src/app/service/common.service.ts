import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  serviceList = [
    {
      id: 1,
      title: 'Appliance Repair',
      description: 'Jugadu baba provides all service and repair for household appliance, How an aliance looks is as important as how efficiently it works',
      icon: 'bx bx-slider-alt',
      top: true,
      order: 3,
      subcategories: [
        { name: 'Ac service and repair' },
        { name: 'ro technician' },
        { name: 'washing machine service and repair' },
        { name: 'refrigerator service and repair' },
        { name: 'microwave repair and service chimney' },
        { name: 'cleaning and repair' },
        { name: 'tv repair,installation and service geyser' },
        { name: 'repair and service' }
      ]
    },
    {
      id: 2,
      title: 'Home Cleaning',
      description: 'We dream of having a clean house – But who dreams of actually doing the cleaning? Our team is here to help you',
      icon: 'bx bx-home-circle',
      subcategories: [
        { name: 'sofa cleaning' },
        { name: 'carpet cleaning' },
        { name: 'kitchen cleaning' },
        { name: 'bathroom cleaning' },
        { name: 'chimney and hob cleaning' },
        { name: 'water tank cleaning' }
      ]
    }, {
      id: 3,
      title: 'Doorstep Carwash',
      description: 'Your car, our shine.” We got experts to wash your car at your door',
      icon: 'bx bx-car',
      subcategories: [
        { name: 'carwash service in apartments' },
        { name: 'individual carwash' },
        { name: 'corporate carwash' },
        { name: 'on-call carwash' }

      ]
    },
    {
      id: 4,
      title: 'Salon at home',
      description: 'Beauty comes from the inside. inside the hair salon. That’s inside your home',
      icon: 'bx bx-cut',
      subcategories: [
        { name: 'haircut at home' },
        { name: 'shave, Beard' },
        { name: 'hair color' },
        { name: 'face massage' },
        { name: 'facial and bleach' },

      ]
    }, {
      id: 5,
      title: 'Real State',
      description: 'Don’t wait to buy real estate, buy real estate and wait. We have several sale,rent,lease and purchase options',
      icon: 'bx bx-building',
      top: true,
      order: 2,
      subcategories: [
        { name: 'flat(buy/rent/lease)' },
        { name: 'house/villa(buy/rent/lease)' },
        { name: 'office space(buy/rent/lease)' },
        { name: 'shop/garage/showroom other' },
        { name: 'commercial' }
      ]
    },
    {
      id: 6,
      title: 'Appliance Rentals',
      description: 'Planing for a temporary settlement? get almost every appliance on rent',
      icon: 'bx bx-book-bookmark',
      subcategories: [
        { name: 'ac on rent' },
        { name: 'tv on rent' },

        { name: 'fridge/refrigerator on rent' }


      ]
    }, {
      id: 7,
      title: 'Car Rentals',
      description: 'Want to take a car for short period of time or can’t afford to buy a caar..we have a jugaar',
      icon: 'bx bxs-taxi',
      top: true,
      order: 4,
      subcategories: [
        { name: 'private cars on rent' },
        { name: 'commercial cars on rent' },
        { name: 'subscribe a car' },
        { name: 'hire a taxi' }

      ]
    },
    {
      id: 8,
      title: 'Pest Control',
      description: 'The first day one is a guest, the second a burden, and the third a pest. Get it cleared with jugadu baba',
      icon: 'bx bx-wind',
      subcategories: [
        { name: 'SANITIZING' },
        { name: 'SERVICE GENERAL' },
        { name: 'PEST CONTROL' },
        { name: 'MOSQUITO FOGGING SERVICE' },
        { name: 'ANTI TERMITE TREATMENT' },
        { name: 'SERVICE BEDBUG CONTROL' },
        { name: 'RAT CONTROL' }
      ]
    }, {
      id: 9,
      title: 'Labour Supply',
      description: 'we supply labours for your work. just call us',
      icon: 'bx bx-wrench',
      subcategories: [
        { name: 'contract basis labours masion' },
        { name: 'cook' },
        { name: 'home servant' },
        { name: 'housekeepers' },
        { name: 'on call labour supply' }
      ]
    },
    {
      id: 10,
      title: 'Manpower Supply',
      description: 'It is the duty of our men to enroll themselves in the your services',
      icon: 'bx bx-sitemap',
      subcategories: [
        { name: 'security guards office' },
        { name: 'staffs corporate staffs' },
        { name: 'apartment management team' },
        { name: 'others' }
      ]
    }, {
      id: 11,
      title: 'Education',
      top: true,
      order: 1,
      description: 'Education is the passport to the future, for tomorrow belongs to those who prepare for it today.',
      icon: 'bx bx-book',
      subcategories: [
        { name: 'aviation' },
        { name: 'admission assisstance' },
        { name: 'medical counselling service' },
        { name: 'ONLINE/OFFLINE FREE COUNSELLING' },
        { name: 'GLOBAL ADMISSION' },
        { name: 'EDUCATION LOAN' },
        { name: 'SELF DEFENCE PROGRAM' }
      ]
    },
    {
      id: 12,
      title: 'Information technology',
      description: 'we are troubleshooters. Call us',
      icon: 'bx bx-laptop',
      subcategories: [
        { name: 'GRAPHIC' },
        { name: 'DESIGN' },
        { name: 'WEBSITE DESIGN' },
        { name: 'ONLINE/OFFLINE ADVERTISING' }

      ]
    }, {
      id: 13,
      title: 'Legal Services',
      description: 'Almost every notary and documentations are done here',
      icon: 'bx bx-user-voice',
      top: true,
      order: 5,
      subcategories: [
        { name: 'ITR' },
        { name: 'GST' },
        { name: 'FILING' },
        { name: 'PAN CARD' },
        { name: 'BUSINESS SETUP' },
        { name: 'TAX AND COMPLIANCE' },
        { name: 'company registeration' },
        { name: 'partnership firm registeration' },
        { name: 'TRADEMARK AND IP' },
        { name: 'NGO' },
        { name: 'property and personal' },
        { name: 'contracts' }


      ]
    },
    {
      id: 14,
      title: 'Packers And Movers',
      description: 'We pack and move your belongings including furniture, suitcases, artifacts, and furnishings, among others safely.',
      icon: 'bx bxs-baby-carriage',
      subcategories: [
        { name: 'INTER-CITY' },
        { name: 'INTER-STATE' },
        { name: 'INTER NATIONAL' }
      ]
    },
    {
      id: 16,
      title: 'Textiles and lines',
      description: 'We solved household and commercial textiles facility',
      icon: 'bx bx-text',
      subcategories: [
        { name: 'Hotel Lines' },
        { name: 'hospital linens' },
        { name: 'household linens' },
        { name: 'matress' },
        { name: 'corporate uniforms on demand' }

      ]
    }, {
      id: 17,
      title: 'Modeling and Photography',
      description: 'If you are beautiful, we will capture your beauty. if you are not then we will make you beautiful.',
      icon: 'bx bxs-camera',
      subcategories: [
        { name: 'portfolio shootout videoshoot' },
        { name: 'music video' },
        { name: 'audio song record' },
        { name: 'modeling shootout' },
        { name: 'wedding shootout' },
        { name: 'festival shoots' }
      ]
    },
    {
      id: 18,
      title: 'Hospitality And Management',
      description: 'We are providing services in mangement field also, Contact Us.',
      icon: 'bx bxs-cool',
      subcategories: [
        { name: 'catering services' },
        { name: 'event management' },
        { name: 'concert booking' },
        { name: 'welfare program' },
        { name: 'cultural program' }
      ]
    }
  ]

  staticContent = [
    {
      title: "About Us",
      id: "/about",

      description: "We have a flexibl approach to problem-solving that uses limited resources in an innovative way.JUGADU BABA SERVICES PRIVATE LIMITED is one of the largest at-home services provider Company. We are an all-in-one platform that helps users hire premium service professionals, from technicians and masseurs to sofa cleaners, carpenter and plumbers.JUGADU BABA is also a technology-driven platform, offering services that cover the legal needs of startups and established businesses that’s why we offer a wide range of services to individuals, such as property agreements and tax filings.You will access this service from our Legal Services Platform.From bike Rentals to Property Rentals, From Labour Supply to Skilled Manpower Supply.we deal with almost everything."
    },
    {
      title: "Privacy Policy",
      id: "/privacy-policy",
      description: "We understands the importance of information about you and your privacy. We conduct our business in compliance with applicable law and have implemented measures to protect your personal information.Our Data Protection Policy helps you to understand how we collect, use, share and protect information about you.Please read this Data Protection Policy to ensure you understand how we handle information about you. Please also check this page from time to time as the Data Protection Policy may be updated.We may assign to your phone or computer one or more cookies to facilitate access to and use of our websites, webpages or mobile applications.A cookie is a simple text file that is stored on your computer or mobile device by a website’s server and only that server will be able to retrieve or read the contents of that cookie. Each cookie is unique to your web browser. It will contain some anonymous information such as a unique identifier and the site name and some digits and numbers. It allows a website to remember things like your preferences or what’s in your shopping basket.Cookies may be essential to enable you to move around our website, webpage or mobile application and use its features, which may not work properly, or at all, if you do not allow these cookies to be used.Cookies may collect information about how visitors use a website, webpage or mobile application, for instance which pages visitors go to most often. These cookies do not collect information that identifies a visitor. All information these cookies collect is aggregated and is therefore anonymous. The information is only used to improve how the website works.Cookies may allow the website, webpage or mobile application to remember choices you make (such as your user name, language or the region you are in) and provide enhanced, more personal features (for example, remembering changes you have made to text size and fonts).You can control information collected by cookies. You can delete or decline cookies by changing your browser settings. Click “help” in the toolbar of most browsers for instructions. You can configure your web browser to reject and delete cookies and block JavaScript. You can control your preferences regarding Flash local stored objects at here.We may partner with third parties to deliver more relevant advertisements and to compile statistics. These third parties may use cookies, web beacons and other tracking technology to monitor your visits to websites, webpages and mobile applications or location information. They monitor your behaviour and identify usage, helping advertisers show you advertising and content that is more relevant to you or for other administrative purposes. They may also measure the overall effectiveness of online or digital advertising, content, programming or other activities.Our systems record a variety of information in relation to your interactions, including information about software versions used, device identifiers (like IP address), location data (where available and not disabled by the user), dates, times, file metadata, referring site, data entered and user activity.We may collect information about your computer, including your internet address, operating system and browser type when you visit us. Your internet address is used to help identify and gather statistical or demographic information about our customers generally. We also use this information for internal system administration to help diagnose problems with our servers, to administer our website and the customer or user experience.Other technologies that may be used include web beacons (which may operate in conjunction with cookies), Flash local stored objects and JavaScript.We may deliver information or notifications to your phone or mobile device. You can disable these notifications by changing the settings in either the app or on your mobile device.We may provide you with the opportunity to inform others electronically about our services, information, content or features, which may require information about the recipient. If you access any of our websites, webpages or mobile applications from a third party social platform, such as Facebook, we may share information with that platform to the extent permitted by your agreement with that platform and its privacy settings. Whenever you voluntarily disclose information on publicly-viewable pages, that information will be publicly available and can be collected and used by others."
    }
    ,
    {
      title: "Terms of Service",
      id: "/terms-and-conditions",
      description: "These Terms & Conditions, and our Data Protection Policy, apply to use of any s or services of Jugadu Baba. They may be updated from time to time at www.jugadubaba.com.Except as otherwise provided, if any provision of these Terms & Conditions is held to be invalid, unlawful or for any reason unenforceable, such provision shall be struck out and shall not affect the validity, legality or enforceability of the remaining provisions. You may not grant a security over or assign or transfer your rights or responsibilities unless we have given our written permission. We may assign or subcontract any right or obligation without your consent, now being given.These Terms & Conditions apply to successors and permitted assigns. Except as otherwise provided, these Terms & Conditions may not be amended except through mutual agreement by you and our representative who intends to amend these Terms & Conditions and is duly authorised to agree to such an amendment. If you act on behalf of a business entity, you represent that you are authorised to act on behalf of such business and bind the business to these Terms & Conditions. No agency, partnership, joint venture, employee-employer or franchiser-franchisee relationship is intended or created by these Terms & Conditions. These Terms & Conditions and any other document or agreement referred to in these Terms & conditions constitute the entire understanding and agreement between you and us and supersede all prior understandings and agreements of the parties. A person who is not a party to these Terms & Conditions has no right under the Contracts (Rights of Third Parties) Act, Chapter 53B to enforce any term of these Terms & Conditions but this does not affect any right or remedy of a third party specified in these Terms & Conditions or which exists or is available apart from that Act.The following sections survive any termination of these Terms & Conditions: Ownership and risk, Collection and delivery, Information, Limited warranties, Taxes and currencies, Failure to make payment, Liability and indemnity, Uncontrollable events, Personal information, General, Governing law and jurisdiction. If we have provided a translation of these Terms & Conditions, we will use the English language version in deciding any issues or disputes which arise. No failure or delay to exercise any right or remedy shall constitute a waiver of that or any other right or remedy, nor shall it prevent or restrict the further exercise of that or any other right or remedy. No single or partial exercise of such right or remedy shall prevent or restrict the further exercise of that or any other right or remedy. Headings are for reference purposes only and do not limit the scope or extent of such section. The word including or similar will be interpreted without limitation.References to the singular include the plural and vice versa."
    },
    {
      title: "Contact Us",
      id: "/contact-us",
      description: "We are a market place for all local professional services. WE help customers get trusted professional to hire for all there service needs. We are staffed with young energetic passionate professional working at your doorsteps to make a difference in the lives of people by catering to there service needs at their time values. We will provide 24*7 support for all of you problems. Contact us on:" + "<br><br>" +
        "<ul>" +
        "<li> <b> Phone: </b> +91 8529750400/7044404845 </li>" +
        "<li> <b> Email: </b> jugadubabaservices@gmail.com </li>" + 
        "<li> <b> Address: </b> Ambika Apt, 1st Floor,FL No. C-217 Lokenath Park P.O.- Hatiyara Kolkata 700157</li>" + 

        "</ul>"
    }

  ];


  constructor() { }

  getData(id) {
    return "hllo Tst";
  }

  getAllData() {
    return this.serviceList;
  }

  getDataById(param) {
    if (param) {
      const data = this.serviceList.filter(data => data.id == param);
      return data[0];
    }
  }
  getTopList() {
    const data = this.serviceList.filter(data => data.hasOwnProperty('top') && data.top == true);
    return data;
  }

  getContent(id) {
    if (id) {
      const data = this.staticContent.filter(data => data.id == id);
      return data[0];
    }
  }

}
